package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ConnectionDB.ConnectionFactory;
import Model.Client;
import Model.OrderDetails;

public class OrderDetailsDAO {
	protected static final Logger LG= Logger.getLogger(ClientDAO.class.getName());
	private static final String SS = "SELECT * from OrderDetails where id=?";
	public static OrderDetails findUsingID(int idF) {
		OrderDetails od=null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectStat = null;
		ResultSet resSet=null;
		try {
			selectStat = dbConnection.prepareStatement(SS);
			selectStat.setLong(1, idF);
			resSet=selectStat.executeQuery();
			resSet.next();
			
			int prID = resSet.getInt("prID");
			int orderID = resSet.getInt("orderID");
			
			od=new OrderDetails(idF,prID,orderID);
		}
		catch(SQLException e) {
			LG.log(Level.WARNING, "ClientDAO:findUsingId" + e.getMessage());
		}
		finally {
			ConnectionFactory.close(resSet);
			ConnectionFactory.close(selectStat);
			ConnectionFactory.close(dbConnection);
		}
		return od;
	}
	
	public static OrderDetails insert(OrderDetails od) {
		Connection dbConnection = null;
		PreparedStatement insertStat = null;
		String instr="INSERT INTO OrderDetails (id,prID,orderID) "+"VALUES("+ od.getId()+", "+od.getPrID()+", "+od.getOrderID()+");";
		int success=0;
		
		try {
			dbConnection=ConnectionFactory.getConnection();
			insertStat=dbConnection.prepareStatement(instr);
			success=insertStat.executeUpdate();
		}
		catch(SQLException e) {
			LG.log(Level.WARNING, "OrderDetailsDAO: insert "+e.getMessage());
		}
		finally {
			ConnectionFactory.close(insertStat);
			ConnectionFactory.close(dbConnection);
		}
		if(success==1) {
			return od;
		}
		return null;
	}
	
	public static OrderDetails update(OrderDetails od,int idF) {
		Connection dbConnection =null;
		PreparedStatement updateStat = null;
		String instr="UPDATE OrderDetails SET prID="+od.getPrID()+", orderID='"+od.getOrderID()+" Where id=?";
	
		int success=0;
		try {
			
			dbConnection=ConnectionFactory.getConnection();
			updateStat=dbConnection.prepareStatement(instr);
			updateStat.setLong(1, idF);
			success=updateStat.executeUpdate();
		}
		catch(SQLException e) {
			LG.log(Level.WARNING, "OrderDetailDAO: update "+e.getMessage());
		}
		finally {
			ConnectionFactory.close(updateStat);
			ConnectionFactory.close(dbConnection);
		}
		if(success==1)
			return od;
		return null;
	}
}
