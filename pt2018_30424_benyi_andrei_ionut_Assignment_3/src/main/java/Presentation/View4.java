package Presentation;
import javax.swing.*;
import java.awt.*;
public class View4 {
	
	private JFrame mainFr;
	public static JTextArea ta;
	public JScrollPane jp;
	
	private JPanel cp1=new JPanel();
	
	public View4() {
		mainFr=new JFrame();
		ta=new JTextArea("");
		mainFr.setLocationRelativeTo(null);
		mainFr.setSize(500, 500);
		ta.setSize(new Dimension(500,500));
		jp=new JScrollPane(ta);
		jp.setPreferredSize(new Dimension(500,500));
		
		cp1.add(jp);
		mainFr.add(cp1);
		mainFr.setVisible(true);
	}
	
	
}
