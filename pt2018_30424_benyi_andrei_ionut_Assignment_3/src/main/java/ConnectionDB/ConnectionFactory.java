package ConnectionDB;

import java.util.logging.*;
import java.util.*;
import java.sql.*;

public class ConnectionFactory {

	private static final Logger LG=Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DATABASEURL="jdbc:mysql://localhost:3306/warehouse";
	private static final String USER="root";
	private static final String PASS="";
	private static final String DR="com.mysql.cj.jdbc.Driver";
	
	private static ConnectionFactory singleInstance=new ConnectionFactory();
	
	private ConnectionFactory() {
		try {
			Class.forName(DR);
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private Connection createConnection() {
		Connection con=null;
		try {
			con=DriverManager.getConnection(DATABASEURL, USER, PASS);
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
		return con;
	}
	
	public static Connection getConnection() {
		return singleInstance.createConnection();
	}
	
	public static void close(Connection con) {
		if(con!=null) {
			try {
				con.close();
			}
			catch(SQLException e) {
				LG.log(Level.WARNING,"Error while closing");
			}
		}
	}
	
	public static void close(Statement st) {
		if(st!=null) {
			try {
				st.close();
			}
			catch(SQLException e) {
				LG.log(Level.WARNING,"Error while closing");
			}
		}
	}
	
	public static void close(ResultSet rs) {
		if(rs!=null) {
			try {
				rs.close();
			}
			catch(SQLException e) {
				LG.log(Level.WARNING,"Error while closing");
			}
		}
	}
}
