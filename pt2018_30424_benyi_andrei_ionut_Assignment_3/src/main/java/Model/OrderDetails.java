package Model;

public class OrderDetails {
	private int id;
	private int prID;
	private int orderID;
	
	public OrderDetails(int i,int prI,int ordI) {
		this.id=i;
		this.prID=prI;
		this.orderID=ordI;
	}
	
	public OrderDetails(int prI,int ordI) {
		this.prID=prI;
		this.orderID=ordI;
	}
	
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id=id;
	}
	
	public int getPrID() {
		return this.prID;
	}
	public void setPrID(int id) {
		this.prID=id;
	}
	
	public int getOrderID() {
		return this.orderID;
	}
	public void setOrderID(int id) {
		this.orderID=id;
	}
}
