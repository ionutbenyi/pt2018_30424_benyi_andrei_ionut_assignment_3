package Model;
/**
 * Class for the Order1 table in the DB
 * @author Andrei Ionut Benyi
 *
 */
public class Order1 {
	private int id;
	private String date;
	
	public Order1(int id, String dat) {
		super();
		this.id=id;
		this.date=dat;
	}
	
	public Order1(String dat) {
		super();
		this.date=dat;
	}
	
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id=id;
	}
	
	public String getDate() {
		return this.date;
	}
	public void setDate(String s) {
		this.date=s;
	}
}
