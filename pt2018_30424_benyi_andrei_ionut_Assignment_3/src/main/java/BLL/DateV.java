package BLL;

import Model.Order1;

public class DateV implements Validator<Order1>{
	public void validate(Order1 c) {
		if(c.getDate().length()>10) {
			throw new IllegalArgumentException("Not a valid date");
		}
	}
}
