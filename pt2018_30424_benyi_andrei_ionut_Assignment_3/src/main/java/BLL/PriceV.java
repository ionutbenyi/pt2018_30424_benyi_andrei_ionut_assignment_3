package BLL;
import Model.Product;
public class PriceV implements Validator<Product>{
	public void validate(Product p) {
		if(p.getPrice()<=0) {
			throw new IllegalArgumentException("Price not valid");
		}
	}
}
