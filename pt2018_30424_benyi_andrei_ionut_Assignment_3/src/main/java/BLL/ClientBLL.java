package BLL;
import java.util.*;
import DAO.ClientDAO;
import Model.Client;

public class ClientBLL {
	private ArrayList<Validator<Client>> v;
	
	public ClientBLL() {
		v=new ArrayList<Validator<Client>>();
		v.add(new EmailV());
		v.add(new PhoneV());
	}
	
	public Client findClientById(int i) {
		Client c=ClientDAO.findUsingID(i);
		if(c==null) {
			throw new NoSuchElementException("no Client with id "+i);
		}
		return c;
	}
	
	public Client insertClient(Client c) {
		for(Validator<Client> vi: v) {
			vi.validate(c);
		}
		
		Client c2=ClientDAO.insert(c);
		return c2;
	}
}
